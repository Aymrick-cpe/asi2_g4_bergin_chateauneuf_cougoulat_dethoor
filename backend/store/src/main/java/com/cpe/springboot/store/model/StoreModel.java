package com.cpe.springboot.store.model;

import fr.cpe.Lib.card.model.CardDTO;
import fr.cpe.Lib.store.model.StoreDTO;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


@Entity
public class StoreModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;

    @ElementCollection
    private Set<Integer> cardList = new HashSet<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Set<Integer> getCardList() {
        return cardList;
    }

    public void setCardList(Set<Integer> cardList) {
        this.cardList = cardList;
    }

    public void addCard(CardDTO card) {
        this.cardList.add(card.getId());
    }

    public StoreDTO toDTO() {
        return new StoreDTO(this.id, this.name, this.cardList);
    }
}
