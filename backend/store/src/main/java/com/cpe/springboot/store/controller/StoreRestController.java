package com.cpe.springboot.store.controller;

import fr.cpe.Lib.store.model.StoreOrder;
import fr.cpe.Lib.store.rest.StoreRest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
public class StoreRestController implements StoreRest {

    @Autowired
    private StoreService storeService;

    @PostMapping(value = "/buy")
    public boolean buyCard(@RequestBody StoreOrder order) {
        return storeService.buyCard(order.getUser_id(), order.getCard_id());
    }

    @PostMapping(value = "/sell")
    public boolean sellCard(@RequestBody StoreOrder order) {
        return storeService.sellCard(order.getUser_id(), order.getCard_id());
    }
}
