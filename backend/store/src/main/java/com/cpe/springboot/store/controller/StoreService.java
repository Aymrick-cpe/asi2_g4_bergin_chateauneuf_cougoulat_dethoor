package com.cpe.springboot.store.controller;

import com.cpe.springboot.store.model.StoreModel;
import fr.cpe.Lib.card.model.CardDTO;
import fr.cpe.Lib.card.rest.CardRestClient;
import fr.cpe.Lib.user.model.UserDTO;
import fr.cpe.Lib.user.rest.UserRestClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;


@Service
public class StoreService {
    @Autowired
    JmsTemplate jmsTemplate;

    @Autowired
    StoreRepository storeRepository;

    private StoreModel store;

    @Autowired
    private CardRestClient cardRestClient;

    @Autowired
    private UserRestClient userRestClient;

    public void generateNewStore(String name, int nb) {
        StoreModel store = new StoreModel();
        store.setName(name);

        List<CardDTO> cardList = cardRestClient.getRandCard(nb);
        for (CardDTO c : cardList) {
            store.addCard(c);
        }

        storeRepository.save(store);
        this.store = store;
    }

    public boolean buyCard(Integer user_id, Integer card_id) {
        UserDTO u = userRestClient.getUser(user_id);
        CardDTO c = cardRestClient.getCard(card_id);
        if (Objects.isNull(u) || Objects.isNull(c)) {
            return false;
        }
        if (u.getAccount() > c.getPrice()) {
            u.addCard(c);
            u.setAccount(u.getAccount() - c.getPrice());
            c.setUser(u.getId());
            cardRestClient.updateCard(c);
            userRestClient.updateUser(u);
            return true;
        } else {
            return false;
        }
    }

    public boolean sellCard(Integer user_id, Integer card_id) {
        UserDTO u = userRestClient.getUser(user_id);
        CardDTO c = cardRestClient.getCard(card_id);
        if (Objects.isNull(u) || Objects.isNull(c)) {
            return false;
        }

        c.setUser(null);
        cardRestClient.updateCard(c);
        u.setAccount(u.getAccount() + c.getPrice());
        userRestClient.updateUser(u);
        return true;
    }

    public Set<Integer> getAllStoreCard() {
        return this.store.getCardList();
    }
}
