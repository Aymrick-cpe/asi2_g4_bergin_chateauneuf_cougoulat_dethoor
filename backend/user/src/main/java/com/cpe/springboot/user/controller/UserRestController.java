package com.cpe.springboot.user.controller;

import com.cpe.springboot.user.model.UserModel;
import fr.cpe.Lib.user.model.AddUserDTO;
import fr.cpe.Lib.user.model.UserDTO;
import fr.cpe.Lib.user.rest.UserRestClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
public class UserRestController implements UserRestClient {

    @Autowired
    private UserService userService;

    @Autowired
    private ModelMapper mapper;

    @GetMapping("/users")
    public List<UserDTO> getAllUsers() {
        List<com.cpe.springboot.user.model.UserModel> users = userService.getAllUsers();
        List<UserDTO> usersDTO = new ArrayList<>();
        for (com.cpe.springboot.user.model.UserModel u : users) {
            usersDTO.add(mapper.modelToDto(u));
        }
        return usersDTO;
    }


    @GetMapping("/user/{id}")
    public UserDTO getUser(@PathVariable Integer id) {
        Optional<UserModel> ruser;
        ruser = userService.getUser(id);

        if (ruser.isPresent()) {
            return mapper.modelToDto(ruser.get());
        }
        return null;

    }

    @PostMapping
    public void addUser(AddUserDTO user) {

        System.out.println(user.toString());
        userService.addUser(new UserModel(user));
    }

    @PutMapping(value = "/user/{id}")
    public void updateUser(@RequestBody UserDTO user, @PathVariable String id) {
        user.setId(Integer.valueOf(id));
        userService.updateUser(user);
    }

    @DeleteMapping(value = "/user/{id}")
    public void deleteUser(@PathVariable Integer id) {
        userService.deleteUser(id);
    }

    @PostMapping(value = "/auth")
    public UserDTO getAllCourses(@RequestBody fr.cpe.Lib.user.model.UserModel user) {
        UserModel u = userService.getUserByLoginPwd(user.getLogin(), user.getPwd()).get(0);
        return u.toDTO();
    }

    @PutMapping(value="/user")
    public boolean updateUser(@RequestBody UserDTO user) {
        return userService.updateUser(user);
    }

}
