package com.cpe.springboot.user.controller;

import com.cpe.springboot.user.Bus.BusListenerService;
import com.cpe.springboot.user.Bus.BusSenderService;
import com.cpe.springboot.user.model.UserModel;
import fr.cpe.Lib.card.model.CardDTO;
import fr.cpe.Lib.card.rest.CardRestClient;
import fr.cpe.Lib.user.model.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BusListenerService busListenerService;

    @Autowired
    private CardRestClient cardRestClient;

    @Autowired
    private BusSenderService busSenderService;

    @Autowired
    private ModelMapper mapper;

    public List<UserModel> getAllUsers() {
        List<UserModel> userList = new ArrayList<>();
        userRepository.findAll().forEach(userList::add);
        return userList;
    }

    public Optional<UserModel> getUser(String id) {
        return userRepository.findById(Integer.valueOf(id));
    }

    public Optional<UserModel> getUser(Integer id) {
        return userRepository.findById(id);
    }

    public void addUser(UserModel user) {
        user.setAccount(100.0F);
        System.out.println("UserService addUser: "+ user.toString());
        busSenderService.sendMsg(user);
    }

    public void addUserWorker(UserModel u){
        UserModel userSaved = userRepository.save(u);
        cardRestClient.affectCardsToUser(5, userSaved.getId());

    }

    public void deleteUser(Integer id) {
        userRepository.deleteById(id);
    }

    public List<UserModel> getUserByLoginPwd(String login, String pwd) {
        List<UserModel> ulist = null;
        ulist = userRepository.findByLoginAndPwd(login, pwd);
        return ulist;
    }

    public boolean updateUser(UserDTO u) {
        System.out.println("LAAAAAA: "+u.toString());
        UserModel user = mapper.dtoToModel(u);
        Optional<UserModel> req = userRepository.findById(user.getId());
        if(req.isPresent()){
            userRepository.save(user);
            return true;
        }
        return false;
    }
}
