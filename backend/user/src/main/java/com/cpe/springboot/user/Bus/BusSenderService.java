package com.cpe.springboot.user.Bus;

import com.cpe.springboot.user.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

@Service
public class BusSenderService {

    @Autowired
    JmsTemplate jmsTemplate;

    public void sendMsg(UserModel u) {
        System.out.println("BusSenderService sendMsq: "+u.toString());
        jmsTemplate.convertAndSend("USER", u);
    }
}
