package com.cpe.springboot.user.controller;

import com.cpe.springboot.user.model.UserModel;
import fr.cpe.Lib.user.model.UserDTO;
import org.springframework.stereotype.Service;

@Service
public class ModelMapper {

    public UserDTO modelToDto(UserModel model){
        UserDTO dto=new UserDTO();
        dto.setId(model.getId());
        dto.setAccount(model.getAccount());
        dto.setLogin(model.getLogin());
        return dto;
    }

    public UserModel dtoToModel(UserDTO dto){
        UserModel model= new UserModel();
        model.setAccount(dto.getAccount());
        model.setId(dto.getId());
        model.setLogin(dto.getLogin());
        return model;
    }
}
