package com.cpe.springboot.user.Bus;

import com.cpe.springboot.user.controller.UserRepository;
import com.cpe.springboot.user.controller.UserService;
import com.cpe.springboot.user.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import javax.jms.Message;
import java.util.List;

@Component
public class BusListenerService {

    @Autowired
    JmsTemplate jmsTemplate;

    @Autowired
    UserService userService;

    @JmsListener(destination = "USER", containerFactory = "connectionFactory")
    public void receiveMessageResult(UserModel u, Message message) {
        System.out.println("BusListenerService receiveMess: "+u.toString());
        userService.addUserWorker(u);
    }
}
