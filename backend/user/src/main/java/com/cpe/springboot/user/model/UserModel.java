package com.cpe.springboot.user.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import fr.cpe.Lib.user.model.AddUserDTO;
import fr.cpe.Lib.user.model.UserDTO;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class UserModel implements Serializable {

    private static final long serialVersionUID = 2733795832476568049L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String login;
    private String pwd;
    private Float account;
    private String lastName;
    private String surName;
    private String email;

    @ElementCollection
	private Set<Integer> cardList = new HashSet<>();

    public UserModel() {
        this.login = "";
        this.pwd = "";
        this.lastName = "lastname_default";
        this.surName = "surname_default";
        this.email = "email_default";
    }

    public UserModel(AddUserDTO addUserDTO){
        this.login = addUserDTO.getLogin();
        this.email = addUserDTO.getEmail();
        this.pwd = addUserDTO.getPwd();
        this.lastName = addUserDTO.getLastName();
        this.surName = addUserDTO.getSurName();
    }

    public UserModel(String login, String pwd) {
        super();
        this.login = login;
        this.pwd = pwd;
        this.lastName = "lastname_default";
        this.surName = "surname_default";
        this.email = "email_default";
    }

    public UserModel(fr.cpe.Lib.user.model.UserModel user) {
        this.id = user.getId();
        this.login = user.getLogin();
        this.pwd = user.getPwd();
        this.account = user.getAccount();
        this.lastName = user.getLastName();
        this.surName = user.getSurName();
        this.email = user.getEmail();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }


	public Set<Integer> getCardList() {
		return cardList;
	}

    public Float getAccount() {
        return account;
    }

    public void setAccount(Float account) {
        this.account = account;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserDTO toDTO() {
        return new UserDTO(this.id, this.login, this.account);
    }

    @Override
    public String toString() {
        return "UserModel{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", pwd='" + pwd + '\'' +
                ", account=" + account +
                ", lastName='" + lastName + '\'' +
                ", surName='" + surName + '\'' +
                ", email='" + email + '\'' +
                ", cardList=" + cardList +
                '}';
    }
}
