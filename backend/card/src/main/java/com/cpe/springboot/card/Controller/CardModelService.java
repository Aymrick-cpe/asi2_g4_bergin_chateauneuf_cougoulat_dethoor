package com.cpe.springboot.card.Controller;

import com.cpe.springboot.card.Bus.BusSenderService;
import com.cpe.springboot.card.model.CardModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CardModelService {

    private Random rand;

    public CardModelService() {
        this.rand = new Random();
    }

    @Autowired
    private CardModelRepository cardRepository;

    @Autowired
    private BusSenderService busSenderService;

    public List<CardModel> getAllCardModel() {
        List<CardModel> cardList = new ArrayList<>();
        cardRepository.findAll().forEach(cardList::add);
        return cardList;
    }

    public void addCard(CardModel cardModel) {
        cardRepository.save(cardModel);
    }

    public void updateCardRef(CardModel cardModel) {
        cardRepository.save(cardModel);

    }

    public void updateCard(CardModel cardModel) {
        System.out.println("SAVE IN DATABASE");
        cardRepository.save(cardModel);
    }

    public Optional<CardModel> getCard(Integer id) {
        return cardRepository.findById(id);
    }

    public void deleteCardModel(Integer id) {
        cardRepository.deleteById(id);
    }

    public List<CardModel> affectCardsToUser(Integer nbr, Integer idUser) {
        List<CardModel> cardList = new ArrayList<>();
        for (int i = 1; i <= nbr; i++) {
            CardModel currentCard = getRandCardRef();
            currentCard.setOwner(idUser);
            currentCard.setAttack(rand.nextFloat() * 100);
            currentCard.setDefence(rand.nextFloat() * 100);
            currentCard.setEnergy(100);
            currentCard.setHp(rand.nextFloat() * 100);
            currentCard.setPrice(111);
            //save new card before sending for user creation
            busSenderService.sendMsg(currentCard);
            cardList.add(currentCard);
        }
        return cardList;
    }

    public CardModel getRandCardRef() {
        Random r = new Random();
        return new CardModel(
                "name",
                "description",
                "family",
                "affinity",
                r.nextFloat(),
                r.nextFloat(),
                r.nextFloat(),
                r.nextFloat(),
                "imgUrl",
                "smallImg",
                r.nextFloat(),
                null
        );
    }



    public List<CardModel> getAllCardToSell() {
        return this.cardRepository.findCardModelsByOwnerIsNull();
    }

    public List<CardModel> getRandCard(int nb) {
        List<CardModel> cardList = new ArrayList<>();
        for (int i = 1; i <= nb; i++) {
            System.out.println(i);
            CardModel currentCard = getRandCardRef();
            currentCard.setAttack(rand.nextFloat() * 100);
            currentCard.setDefence(rand.nextFloat() * 100);
            currentCard.setEnergy(100);
            currentCard.setHp(rand.nextFloat() * 100);
            currentCard.setPrice(111);
            //save new card before sending for user creation
            busSenderService.sendMsg(currentCard);
            cardList.add(currentCard);
        }
        return cardList;
    }

    public void save(CardModel c) {
        cardRepository.save(c);
    }
}

