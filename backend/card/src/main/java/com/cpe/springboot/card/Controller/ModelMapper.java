package com.cpe.springboot.card.Controller;

import com.cpe.springboot.card.model.CardModel;
import fr.cpe.Lib.card.model.CardDTO;
import org.springframework.stereotype.Service;

@Service
public class ModelMapper {

    public CardDTO modelToDto(CardModel model){
        CardDTO dto=new CardDTO();
        dto.setId(model.getId());
        dto.setAttack(model.getAttack());
        dto.setDefence(model.getDefence());
        dto.setEnergy(model.getEnergy());
        dto.setHp(model.getHp());
        dto.setPrice(model.getPrice());
        dto.setUser(model.getOwner());
        return dto;
    }

    public CardModel dtoToModel(CardDTO dto){
        CardModel model= new CardModel();
        model.setId(dto.getId());
        model.setPrice(dto.getPrice());
        model.setEnergy(dto.getEnergy());
        model.setHp(dto.getHp());
        model.setDefence(dto.getDefence());
        model.setAttack(dto.getAttack());
        model.setDefence(dto.getDefence());
        model.setOwner(dto.getUser());
        return model;
    }
}
