package com.cpe.springboot.card.Controller;

import com.cpe.springboot.card.model.CardModel;
import fr.cpe.Lib.card.model.CardDTO;
import fr.cpe.Lib.card.rest.CardRestClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
public class CardRestController implements CardRestClient {


    @Autowired
    private CardModelService cardModelService;

    @Autowired
    private ModelMapper mapper;

    @RequestMapping("/cards")
    public List<CardDTO> getCards() {
        List<CardDTO> cLightList = new ArrayList<>();
        for (CardModel c : cardModelService.getAllCardModel()) {
            System.out.println(c.toString());
            cLightList.add(mapper.modelToDto(c));
        }
        return cLightList;
    }

    @RequestMapping("/card/{id}")
    public CardDTO getCard(@PathVariable Integer id) {
        Optional<CardModel> rcard;
        rcard = cardModelService.getCard(Integer.valueOf(id));
        if (rcard.isPresent()) {
            return mapper.modelToDto(rcard.get());
        }
        return null;

    }

    @RequestMapping("/affectCardsToUser/{nb}/user/{idUser}")
    public List<CardDTO> affectCardsToUser(@PathVariable Integer nb, @PathVariable Integer idUser){
        if (nb > 0) {
            List<CardModel> cards = cardModelService.affectCardsToUser(nb, idUser);
            List<CardDTO> cardsDTO = new ArrayList<>();
            for (CardModel c : cards) {
                cardsDTO.add(mapper.modelToDto(c));
            }
            return cardsDTO;
        }
        return null;
    }

    @PutMapping(value = "/card")
    public void updateCard(@RequestBody CardDTO c) {
        System.out.println("CONTROLLER RECEIVE: " +c.toString());
        CardModel cardModel = mapper.dtoToModel(c);
        System.out.println("CONVERT DTO TO MODEL: "+cardModel.toString());
        cardModelService.updateCard(cardModel);
    }

    @Override
    public List<CardDTO> getRandCard(int nb) {
        if (nb > 0) {
            List<CardModel> cards = cardModelService.getRandCard(nb);
            List<CardDTO> cardsDTO = new ArrayList<>();
            for (CardModel c : cards) {
                cardsDTO.add(mapper.modelToDto(c));
            }
            return cardsDTO;
        }
        return null;
    }

    @PostMapping(value = "/card")
    public void addCard(@RequestBody CardModel card) {
        cardModelService.addCard(card);
    }

    @DeleteMapping(value = "/card/{id}")
    public void deleteUser(@PathVariable String id) {
        cardModelService.deleteCardModel(Integer.valueOf(id));
    }

    @RequestMapping("/cards_to_sell")
    private List<CardModel> getCardsToSell() {
        List<CardModel> list = new ArrayList<>();
        for (CardModel c : cardModelService.getAllCardToSell()) {
            list.add(c);
        }
        return list;
    }


}
