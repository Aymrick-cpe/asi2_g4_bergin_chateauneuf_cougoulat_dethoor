package com.cpe.springboot.card.Bus;

import com.cpe.springboot.card.model.CardModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

@Service
public class BusSenderService {

    @Autowired
    JmsTemplate jmsTemplate;

    public void sendMsg(CardModel c) {
        jmsTemplate.convertAndSend("CARD", c);
    }
}
