package com.cpe.springboot.card.model;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "CardModel")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class CardModel implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private String description;
    private String family;
    private String affinity;
    private String imgUrl;
    private String smallImgUrl;
    private Float energy;
    private Float hp;
    private Float defence;
    private Float attack;
    private Float price;
    private Integer owner;

    //@ManyToOne(fetch = FetchType.LAZY)
    //@ManyToOne(cascade = CascadeType.ALL)
    //@JoinColumn(name = "user_id", nullable = true)
	/*
	@ManyToOne
	@JoinColumn
	private UserModel user;

	@ManyToOne
	@JoinColumn
	private StoreModel store;*/

    public CardModel() {
    }

    /*public CardModel(CardReference cardRef) {
        this.name = cardRef.getName();
        this.description = cardRef.getDescription();
        this.family = cardRef.getFamily();
        this.affinity = cardRef.getAffinity();
        this.imgUrl = cardRef.getImgUrl();
        this.smallImgUrl = cardRef.getSmallImgUrl();
    }*/

    public CardModel(String name, String description, String family, String affinity, float energy, float hp,
                     float defence, float attack, String imgUrl, String smallImg, float price, Integer user) {
        this.name = name;
        this.description = description;
        this.family = family;
        this.affinity = affinity;
        this.imgUrl = imgUrl;
        this.smallImgUrl = smallImg;
        this.energy = energy;
        this.hp = hp;
        this.defence = defence;
        this.attack = attack;
        this.price = price;
        this.owner = user;
        //STOREDTO ??
    }

    public float getEnergy() {
        return energy;
    }

    public void setEnergy(float energy) {
        this.energy = energy;
    }

    public float getHp() {
        return hp;
    }

    public void setHp(float hp) {
        this.hp = hp;
    }

    public float getDefence() {
        return defence;
    }

    public void setDefence(float defence) {
        this.defence = defence;
    }

    public float getAttack() {
        return attack;
    }

    public void setAttack(float attack) {
        this.attack = attack;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }


    public float computePrice() {
        return this.hp * 20 + this.defence * 20 + this.energy * 20 + this.attack * 20;
    }

    public void setOwner(Integer user) {
        this.owner = user;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getAffinity() {
        return affinity;
    }

    public void setAffinity(String affinity) {
        this.affinity = affinity;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getSmallImgUrl() {
        return smallImgUrl;
    }

    public void setSmallImgUrl(String smallImgUrl) {
        this.smallImgUrl = smallImgUrl;
    }

    public void setEnergy(Float energy) {
        this.energy = energy;
    }

    public void setHp(Float hp) {
        this.hp = hp;
    }

    public void setDefence(Float defence) {
        this.defence = defence;
    }

    public void setAttack(Float attack) {
        this.attack = attack;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Integer getOwner() {
        return owner;
    }



    @Override
    public String toString() {
        return "CardModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", family='" + family + '\'' +
                ", affinity='" + affinity + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", smallImgUrl='" + smallImgUrl + '\'' +
                ", energy=" + energy +
                ", hp=" + hp +
                ", defence=" + defence +
                ", attack=" + attack +
                ", price=" + price +
                ", owner=" + owner +
                '}';
    }
}
