package com.cpe.springboot.card.Bus;

import com.cpe.springboot.card.Controller.CardModelRepository;
import com.cpe.springboot.card.Controller.CardModelService;
import com.cpe.springboot.card.model.CardModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import javax.jms.Message;

@Component
public class BusListenerService {

    @Autowired
    JmsTemplate jmsTemplate;

    @Autowired
    CardModelRepository cardRepository;

    @Autowired
    CardModelService cardModelService;

    @JmsListener(destination = "CARD", containerFactory = "connectionFactory")
    public void receiveMessageResult(CardModel c, Message message) {
        System.out.println("BUS LISTENER CARD CREATE : "+c.toString());
        cardModelService.save(c);
    }
}
