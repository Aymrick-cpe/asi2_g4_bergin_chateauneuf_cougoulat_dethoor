package fr.cpe.Lib.user.model;

import fr.cpe.Lib.card.model.CardDTO;

import java.util.HashSet;
import java.util.Set;

public class UserDTO {

    private Integer id;
    private String login;
    private Float account;
    private Set<CardDTO> cardList = new HashSet<>();

    public UserDTO() {
    }

    public UserDTO(Integer id, String login, Float account) {
        this.id = id;
        this.login = login;
        this.account = account;
    }


    public Float getAccount() {
        return account;
    }

    public void setAccount(Float account) {
        this.account = account;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return this.login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void addCard(CardDTO card) {
        this.cardList.add(card);
        card.setUser(this.id);
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", account=" + account +
                ", cardList=" + cardList +
                '}';
    }
}
