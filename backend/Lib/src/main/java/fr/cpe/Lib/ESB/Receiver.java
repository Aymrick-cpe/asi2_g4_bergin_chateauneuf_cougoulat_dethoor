package fr.cpe.Lib.ESB;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.stereotype.Service;

import javax.jms.*;


@Service
public class Receiver implements Runnable {

    private MessageConsumer consumer;
    private Session session;
    private Connection connection = null;

    public Receiver(String brokerURL, String queueName) throws JMSException {
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(brokerURL);
        connection = connectionFactory.createConnection();
        session = connection.createSession(false,
                Session.AUTO_ACKNOWLEDGE);
        Queue queue = session.createQueue(queueName);
        consumer = session.createConsumer(queue);
        connection.start();
    }

    @Override
    public void run() {

        TextMessage textMsg = null;
        try {
            while (true) {
                textMsg = (TextMessage) consumer.receive();
                System.out.println(textMsg);
                String message = textMsg.getText();
                System.out.println("Received: " + message);

            }
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
