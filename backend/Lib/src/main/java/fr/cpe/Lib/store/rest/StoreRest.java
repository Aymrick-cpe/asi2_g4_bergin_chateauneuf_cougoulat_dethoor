package fr.cpe.Lib.store.rest;

import fr.cpe.Lib.store.model.StoreOrder;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = "store", url = "http://store:8088/")
public interface StoreRest {
    @PostMapping(value = "/buy")
    boolean buyCard(@RequestBody StoreOrder order);

    @PostMapping(value = "/sell")
    boolean sellCard(@RequestBody StoreOrder order);
}
