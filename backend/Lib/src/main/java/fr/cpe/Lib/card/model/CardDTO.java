package fr.cpe.Lib.card.model;

import java.io.Serializable;

public class CardDTO implements Serializable {

    private Integer id;
    private Float price;
    private Integer user;
    private Float energy;
    private Float hp;
    private Float defence;
    private Float attack;

    public CardDTO() {
    }

    public CardDTO(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Integer getUser() {
        return user;
    }

    public void setUser(Integer user) {
        this.user = user;
    }


    public Float getEnergy() {
        return energy;
    }

    public void setEnergy(Float energy) {
        this.energy = energy;
    }

    public Float getHp() {
        return hp;
    }

    public void setHp(Float hp) {
        this.hp = hp;
    }

    public Float getDefence() {
        return defence;
    }

    public void setDefence(Float defence) {
        this.defence = defence;
    }

    public Float getAttack() {
        return attack;
    }

    public void setAttack(Float attack) {
        this.attack = attack;
    }

    public float computePrice() {
        return this.hp * 20 + this.defence * 20 + this.energy * 20 + this.attack * 20;
    }

    @Override
    public String toString() {
        return "CardDTO{" +
                "id=" + id +
                ", price=" + price +
                ", user=" + user +
                ", energy=" + energy +
                ", hp=" + hp +
                ", defence=" + defence +
                ", attack=" + attack +
                '}';
    }
}
