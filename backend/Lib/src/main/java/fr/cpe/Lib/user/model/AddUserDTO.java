package fr.cpe.Lib.user.model;

import java.io.Serializable;

public class AddUserDTO implements Serializable {

    private String login;
    private String pwd;
    private String lastName;
    private String surName;
    private String email;

    public AddUserDTO() {
        this.login = "";
        this.pwd = "";
        this.lastName = "lastname_default";
        this.surName = "surname_default";
        this.email = "email_default";
    }

    public AddUserDTO(String login, String pwd) {
        super();
        this.login = login;
        this.pwd = pwd;
        this.lastName = "lastname_default";
        this.surName = "surname_default";
        this.email = "email_default";
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "AddUserDTO{" +
                "login='" + login + '\'' +
                ", pwd='" + pwd + '\'' +
                ", lastName='" + lastName + '\'' +
                ", surName='" + surName + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
