package fr.cpe.Lib.user.rest;

import fr.cpe.Lib.user.model.AddUserDTO;
import fr.cpe.Lib.user.model.UserDTO;
import fr.cpe.Lib.user.model.UserModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(value = "user", url = "http://user:8082/")
public interface UserRestClient {
    @RequestMapping("/users")
    List<UserDTO> getAllUsers();

    @RequestMapping("/user/{id}")
    UserDTO getUser(@PathVariable Integer id);

    @PostMapping(value = "/user")
    void addUser(@RequestBody AddUserDTO user);

    @DeleteMapping(value = "/user/{id}")
    void deleteUser(@PathVariable Integer id);

    @PostMapping(value = "/auth")
    UserDTO getAllCourses(@RequestBody UserModel user);

    @PutMapping(value="/user")
    boolean updateUser(@RequestBody UserDTO user);
}
