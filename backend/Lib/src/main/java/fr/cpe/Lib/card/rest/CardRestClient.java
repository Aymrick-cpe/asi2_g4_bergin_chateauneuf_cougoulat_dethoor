package fr.cpe.Lib.card.rest;

import fr.cpe.Lib.card.model.CardDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(value = "card", url = "http://card:8087/")
public interface CardRestClient {
    @GetMapping("/list")
    List<CardDTO> getCards();

    @RequestMapping("/card/{id}")
    CardDTO getCard(@PathVariable Integer id);

    @RequestMapping("/affectCardsToUser/{nb}/user/{idUser}")
    List<CardDTO> affectCardsToUser(@PathVariable Integer nb, @PathVariable Integer idUser);

    @PutMapping(value = "/card")
    void updateCard(@RequestBody CardDTO c);

    @RequestMapping("/card/getRandCard/{nb}")
    List<CardDTO> getRandCard(int nb);
}
