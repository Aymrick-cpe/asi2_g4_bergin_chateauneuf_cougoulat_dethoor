package fr.cpe.Lib.store.model;

import fr.cpe.Lib.card.model.CardDTO;

import java.util.HashSet;
import java.util.Set;

public class StoreDTO {
    private Integer id;
    private String name;
    private Set<Integer> cardList = new HashSet<>();

    public StoreDTO() {
    }

    public StoreDTO(Integer id, String name, Set<Integer> cardList) {
        this.id = id;
        this.name = name;
        this.cardList = cardList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Integer> getCardList() {
        return cardList;
    }

    public void setCardList(Set<Integer> cardList) {
        this.cardList = cardList;
    }
}
