import express from 'express';
import cors from 'cors';
import http from 'http';
import { Server } from 'socket.io';

import routes from './routes/index.js';

const app = express();
const port = 8000;
const server = http.createServer(app);
const io = new Server(server, {
    cors: {
        origin: "http://localhost:3000"
    }
});

// enable CORS - Cross Origin Resource Sharing
const corsOptions = {
    origin: 'http://localhost:3000',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

app.use(cors(corsOptions));

app.use('/api', routes);

// Socket

let users = [];

function setUsers(usr) {
    users= usr;
}

function getUsers() {
    return users;
}

io.on("connection", (socket) => {
    console.log('New user connected');


    socket.on('login', (data) => {
        socket.username = data.username;
        let temp = getUsers();
        temp.push({
            userID: socket.id,
            username: socket.username,
        });
        setUsers(temp);

        io.emit('newConnectedUser', data.username);
    })

    socket.on("getUsers", username => {
        const res = getUsers().filter(el => el.username != username);
        console.log("Pass on getUsers");
        console.log('usersSend : ', res);
        io.emit("users", res);
    })

    socket.on('setRoom', (data) => {
        console.log("new Room : ", data);
        socket.join(data);
    })
    
    socket.on('chat-log', (data) => {
        console.log("message : ", data);
        io.to(data.room).emit("chat", data);
    })

    socket.on("disconnect", () => {
        const temp = getUsers().filter(el => el.username != socket.username);
        setUsers(temp);
        socket.leave();
        console.log("Client disconnected");
    });
});

server.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
})