import express from 'express';

// Routes imports
import userRoutes from './user/user.route.js';

//
const router = express.Router();

// Init root
router.use('/user', userRoutes);

export default router;