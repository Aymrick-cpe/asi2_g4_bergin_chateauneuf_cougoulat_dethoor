// imports
import express from 'express';
import userCtrl from './user.controller.js';

const router = express.Router();

router.route('/hello').get(userCtrl.getHelloWorld);

export default router;