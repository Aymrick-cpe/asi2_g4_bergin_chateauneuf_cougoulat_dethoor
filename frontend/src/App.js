import './App.css';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import globalReducer from './store/reducers';

import { Login } from "./components/login/Login";
import { Signup } from "./components/signup/Signup";
import { Menu } from "./components/menu/Menu"
import { Card } from './components/card/Card';
import { Header } from "./components/header/Header";
import { GuardianRouter } from './utils/GuardianRouter';
import { GameChat } from './components/game/game-chat/GameChat';

const store = createStore(globalReducer);

function App() {
  return (
    <div className="App">
      <Provider store={store}>
        <BrowserRouter>
          <Routes>
            <Route path="/">
              <Route path="login" element={<Login></Login>} />
              <Route path="signup" element={<Signup></Signup>} />
              <Route index element={
                <GuardianRouter>
                  <Header action="" />
                  <Menu />
                </GuardianRouter>} />
              <Route path="card">
                <Route path="buy" element={
                  <GuardianRouter>
                    <Header action="buy"/>
                    <Card action="buy"/>
                  </GuardianRouter>
                  } />
                <Route path="sell" element={
                  <GuardianRouter>
                    <Header action="sell"/>
                    <Card action="sell"/>
                  </GuardianRouter>
                  } />
              </Route>
              <Route path="game">
                <Route path="chat" element={
                  <GuardianRouter>
                    <Header action="" />
                    <GameChat />
                  </GuardianRouter>
                }>
                </Route>
              </Route>
              <Route
                path="*"
                element={
                  <main style={{ padding: "1rem" }}>
                    <p>There's nothing here!</p>
                  </main>
                }
              />
            </Route>
          </Routes>
        </BrowserRouter>
      </Provider>
    </div>
  );
}

export default App;
