import { useState } from "react";
import { useNavigate } from "react-router-dom"

import TextField from '@mui/material/TextField';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import './Signup.css';

import { createUser } from "../../service/user.service";

export function Signup() {

    const navigate = useNavigate();
    
    // Mock data
    const [ userSignup, setUserSignup ] = useState({
        name: "",
        firstname: "",
        email: "",
        username: "",
        password: "",
        repassword: "",
        wallet: 500,
    });

    const processInput = (event) => {
        const target = event.currentTarget;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        let currentVal = userSignup;
        setUserSignup({...userSignup, [name]: value});
        currentVal[name]= value;
    }

    const submit = async () => {
        console.log(userSignup);

        let user = {
            login: userSignup.username,
            pwd: userSignup.password,
            lastName: userSignup.name,
            surName: userSignup.firstname,
            email: userSignup.email
        }
 
        let redirectAfterSignup = (res) => {
            if (res?.status === 200) {
                navigate("/login");
            }
        }

        await createUser(user, redirectAfterSignup);

        // Envoie info dans db puis redirection
        // navigate("/login");
        // Sinon afficher erreur :

    }

    return (
        <Card sx={{ maxWidth: "50%", marginTop: 24 }} className="center">
            <CardContent>
                <Typography variant="h5" component="div">
                    Inscription
                </Typography>
                <Box sx={{ flexDirection: 'column'}}>
                    <div> 
                        <TextField sx={{ width: "40%", mx: 4, marginTop: 6 }} label="Prénom" variant="outlined" name="firstname" onChange={processInput} value={userSignup.firstname} />
                        <TextField sx={{ width: "40%", mx: 4, marginTop: 6 }} label="Nom" variant="outlined" name="name" onChange={processInput} value={userSignup.name} />
                    </div>
                </Box>
                <Box sx={{ flexDirection: 'column'}}>
                    <div> 
                        <TextField sx={{ width: "40%", mx: 4, marginTop: 6 }} label="E-mail" variant="outlined" name="email" onChange={processInput} value={userSignup.email} />
                        <TextField sx={{ width: "40%", mx: 4, marginTop: 6 }} label="Identifiant" variant="outlined" name="username" onChange={processInput} value={userSignup.username} />
                    </div>
                </Box>
                <Box sx={{ flexDirection: 'column' }}>
                    <div sx={{ marginTop: 6}}>
                        <TextField sx={{ width: "40%", mx: 4, marginTop: 6 }} label="Mot de passe" variant="outlined" name="password" type="password" onChange={processInput} value={userSignup.password} />
                        <TextField sx={{ width: "40%", mx: 4, marginTop: 6 }} label="Confirmez votre mot de passe" variant="outlined" name="repassword" type="password" onChange={processInput} value={userSignup.repassword} />
                    </div>
                </Box>
            </CardContent>
            <CardActions sx={{ flexDirection: 'column' }}>
                <Button sx={{ marginTop: 6, width: "50%" }} size="medium" variant="contained" onClick={submit}>S'inscrire</Button>
                <Button sx={{ marginTop: 2, marginBottom: 4, width: "50%" }} size="medium" variant="contained" onClick={() => navigate("/login")}>Annuler</Button>
            </CardActions>
        </Card>
    )
}
