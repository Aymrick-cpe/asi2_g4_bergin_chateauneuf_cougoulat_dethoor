import { React } from "react";
import { useNavigate } from "react-router-dom"

import Paper from '@mui/material/Paper';

export function MenuItem(props) {
    const action = props.action;
    const path = `assets/${action}.png`;
    const navigate = useNavigate()

    function getURL() {
        switch (props.action) {
            case 'play': return "game/chat";
            case 'buy': return "card/buy";
            case 'sell': return "card/sell";
            default: return "";
        }
    }

    return (
        <Paper sx={{ direction: "column", padding: 8, cursor: "pointer" }} variant="outlined" square elevation={0} onClick={() => navigate(getURL())}>
            <img src={path} alt={action} width="100px"/>
            <div>
                {action.toUpperCase()}
            </div>
        </ Paper>
    )
}
