import { React } from "react";
import { MenuItem } from "./MenuItem";

import Box from '@mui/material/Box';

export function Menu(props) {
    const PLAY = 'play';
    const BUY = 'buy';
    const SELL = 'sell';

    return (
        <div>
            <Box sx={{ marginTop: 16, width: "20%", mx: "auto" }}>
                <MenuItem action={PLAY}/>
            </Box>
            <Box sx={{ marginTop: 16, mx:"auto", display: "flex", flexDirection: "row", justifyContent: 'center' }}> 
                <Box sx={{ width: "20%", mx: 8 }}>  
                    <MenuItem action={BUY}/>
                </Box>
                <Box sx={{ width: "20%", mx: 8 }}> 
                    <MenuItem action={SELL}/>
                </Box>
            </Box>
        </div>
    )
}
