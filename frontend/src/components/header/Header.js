import {React, useState} from "react";
import { updateUser } from "../../store/actions";
import { useNavigate } from "react-router-dom"
import { useSelector, useDispatch } from "react-redux";

import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import AccountCircle from '@mui/icons-material/AccountCircle';
import MenuItem from '@mui/material/MenuItem';
import Menu from '@mui/material/Menu';

export function Header(props) {

    const current_user = useSelector(state => state.userReducer.user);
    const [anchorEl, setAnchorEl] = useState(null);
    const navigate = useNavigate();
    const dispatch = useDispatch();
    let action = props.action;

    function getTitle() {
        switch(action) {
            case 'buy' :
                return "Achat de carte";
            case 'sell':
                return "Vente de carte";
            default: 
                return '';
        }       
    }

    const handleMenu = (event) => {
        setAnchorEl(event.currentTarget);
      };
    
      const handleClose = () => {
        setAnchorEl(null);
      };

    function disconnect() {
        handleClose();
        dispatch(updateUser({}));
        navigate("/login");
    }

    return (
        <div>
            <Box sx={{ flexGrow: 1 }}>
                <AppBar position="static" enableColorOnDark>
                    <Toolbar>
                        <Typography variant="h6" component="div" sx={{ flexGrow: 1 }} edge="start" color="inherit">
                            {current_user.account} €
                        </Typography>
                        <Typography variant="h4" component="div" sx={{ flexGrow: 6 }}>
                            {getTitle()}
                        </Typography>
                        <div>
                            <IconButton
                                size="large"
                                aria-label="account of current user"
                                aria-controls="menu-appbar"
                                aria-haspopup="true"
                                onClick={handleMenu}
                                color="inherit"
                            >
                                <AccountCircle /> 
                                <Typography variant="h6" component="div" sx={{ flexGrow: 1, mx: 2 }}>
                                    {current_user.username}
                                </Typography>
                            </IconButton>
                            <Menu
                                id="menu-appbar"
                                anchorEl={anchorEl}
                                anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'right',
                                }}
                                keepMounted
                                transformOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'right',
                                }}
                                open={Boolean(anchorEl)}
                                onClose={handleClose}
                            >
                                <MenuItem onClick={disconnect}>Se déconnecter</MenuItem>
                            </Menu>
                        </div>
                    </Toolbar>
                </AppBar>
            </Box>
        </div>
        
    )
}
