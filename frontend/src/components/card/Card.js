import { React, useState } from "react";
import { useNavigate } from "react-router-dom"
import { CardInfo } from "./card-info/CardInfo";
import { CardList } from "./card-list/CardList";

import { Box, Button, Alert, Snackbar } from "@mui/material";
import { actionCard } from "../../service/card.service";
import { useSelector } from "react-redux";

export function Card(props) {

    const [card, setCard] = useState({});
    const [open, setOpen] = useState(false);
    const [errMessage, SetErrMessage] = useState("");
    const current_user = useSelector(state => state.userReducer.user);
    const navigate = useNavigate();
    const action = props.action;

    function getTitle() {
        switch(action) {
            case 'buy':
                return "acheter";
            case 'sell':
                return "vendre";
            default :
                return '';
        }
    }

    function getCard() {
        if (Object.keys(card).length === 0 && card.constructor === Object) {
            return (<div>Aucune carte sélectionné...</div>)
        } else {
            return (<div>
                    <CardInfo card={card}/>
                    <Button sx={{ my: 2, width: "100%", flex: 1 }} variant="contained" onClick={() => cardAction()}>{getTitle()} ( {card.price } € )</Button>
                </div>);
        }
    }

    function cardAction() {
        if (card.id) {
            console.log("carte fait des trucs");
            actionCard(current_user.id, card.id, action);
            navigate("/");
        } else {
            SetErrMessage("Vous n'avez pas sélectionné de carte à "+getTitle());
            setOpen(true);
        }
    }
    
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpen(false);
    };

    return (
        <div>
            <Box sx={{ display: "flex", flexDirection: 'row', my: 4, mx: 4 }}>
                <Box width="75%" sx={{ marginRight: 4 }}>
                    <CardList onSelectCard={(card) => setCard(card)} action={props.action}/>
                    <Button variant="text"  onClick={() => navigate("/")}>Retour au menu</Button>
                </Box>
                <Box width="25%">
                    { getCard() }
                </Box>
            </Box>
            <Snackbar open={open} autoHideDuration={3000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="error" sx={{ width: '100%' }}>{errMessage}</Alert>
            </Snackbar>
        </div>
    )
}
