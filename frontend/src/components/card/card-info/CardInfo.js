import { React } from "react";
import { Icon, Grid, Card, CardActions, CardHeader } from "@mui/material";

import './CardInfo.css';

export function CardInfo(props) {

    const card = props.card;

    return (
        <div>
            <Card>
                <CardHeader
                    title={"Carte n°"+card.id}
                />
                <CardActions disableSpacing>
                    <Grid container spacing={2}>
                        <Grid item xs={5}>
                            <Icon>favorite</Icon> {card.hp}
                        </Grid>
                        <Grid item xs={5}>
                            <Icon>handyman</Icon> {card.attack}
                        </Grid>
                        <Grid item xs={5}>
                            <Icon>battery_full</Icon> {card.energy}
                        </Grid>
                        <Grid item xs={5}>
                            <Icon>shield</Icon> {card.defence}
                        </Grid>
                    </Grid>
                </CardActions>
            </Card>
        </div>
    )
}
