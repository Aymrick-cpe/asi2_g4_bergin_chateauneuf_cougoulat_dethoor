import { React, useEffect, useState } from "react";
import { DataGrid } from '@mui/x-data-grid';
import { getCards } from "../../../service/card.service";
import { useSelector } from "react-redux";

export function CardList(props) {
    const [cards, setCards] = useState([]);
    const [event, setE] = useState(null);
    const current_user = useSelector(state => state.userReducer.user);

    const columns = [
        { 
            field: 'id', 
            headerName: 'ID',
            minWidth: 80,
            flex: 1,
        }, {
            field: 'energy',
            headerName: 'Energie',
            minWidth: 80,
            flex: 1,
        }, {
            field: 'hp',
            headerName: 'HP',
            minWidth: 80,
            flex: 1,
        }, {
            field: 'price',
            headerName: 'Prix',
            minWidth: 80,
            flex: 1,
        }
    ];

    useEffect(() => {
        if (event !== null) {
            const card = event.row;
            props.onSelectCard(card);
        }

        const callbackList = (data) => {
            console.log(data);
            setCards(data);
        } 
        console.log(current_user.id);
        getCards(props.action, current_user.id, callbackList);

    }, [event]);
    
    function onSelectRow(e) {
        setE(e);
    }



    return (
        <div style={{ height: 400, width: '100%' }}>
            <DataGrid
                autoHeight 
                rows={cards}
                columns={columns}
                rowsPerPageOptions={[10]}
                onRowClick={onSelectRow}
            />
        </div>
    )
}
