import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import moment from 'moment';

import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import TextField from '@mui/material/TextField';
// import Typography from '@mui/material/Typography';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import SendIcon from '@mui/icons-material/Send';

export function GameChat() {

    const [messages, setMessages] = useState({all: []});
    const [room, setRoom] = useState("all");
    const [rooms, setRooms] = useState([{name: "all", user1: {}, user2: {}}]);
    const userConnected = useSelector(state => state.userReducer.user);
    const socket = useSelector(state => state.socketReducer.io);

    // Mock data
    const [ msgToSend, setMsgToSend ] = useState({
        content: "",
        date: null,
        author: userConnected,
        room: "",
    });

    
    useEffect(() => {
        socket.emit("getUsers", userConnected.username);

        socket.on("users", (users) => {
            console.log("Users : ", users);
        
            const listUser = users.sort();
            
            let temp = rooms;
            for (let el of listUser) {
                let room_names = [userConnected.username, el.username];
                console.log(el);
                room_names.sort();
                const isRoomExist = rooms.find(el => el.name === `${room_names[0]}-${room_names[1]}`);
                if (isRoomExist === undefined) {
                    temp.push({
                        name: `${room_names[0]}-${room_names[1]}`,
                        user1: userConnected,
                        user2: el,  
                    });
                }
            }
            console.log(temp);
            for (let room of rooms) {
                socket.emit('setRoom', room.name);
            }

            setRooms([...temp]);
        })

        // socket.on("newConnectedUser", data => {
        //     console.log(data);
        //     let room_names = [userConnected.username, data.username];
        //     console.log(data);
        //     room_names = room_names.sort((a, b) => {a.localeCompare(b)});
        //     console.log(room_names)
        //     let temp = rooms;
        //     temp.push({
        //         name: `${room_names[0]}-${room_names[1]}`,
        //         user1: userConnected,
        //         user2: data,  
        //     });
        //     setRooms(temp);
        // });

        socket.on('chat', msg => {
            console.log(msg);
            let temp = messages;
            console.log(messages);
            (temp[msg.room]).push(msg);
            setMessages({ ...temp });
        });

            
        
    }, []);

    const processInput = (event) => {
        const target = event.currentTarget;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        setMsgToSend({...msgToSend, content: value, date: new Date()});
    }

    const sendMessage = () => {
        msgToSend.room = room;
        socket.emit('chat-log', msgToSend);
    }

    const changeRoom = (name) => {
        console.log(messages[name]);
        if (messages[name] === undefined) {
            let temp = messages;
            temp[name] = [];
            setMessages({...temp});
        }
        setRoom(name);
    }

    return (
        <Box sx={{ mx: 4, my: 4, height: "600px", maxHeight: "600px"}}>
            <Grid container component={Paper} className="chatSection" sx={{ height:"100%" }}>
                <Grid item xs={3} sx={{borderRight: 1, borderColor: 'lightblue'}}>
                    <List>
                        <ListItem button key="Avatar">
                            <ListItemIcon>
                            <Avatar alt={userConnected.username} src="https://material-ui.com/static/images/avatar/1.jpg" />
                            </ListItemIcon>
                            <ListItemText primary={userConnected.username}></ListItemText>
                        </ListItem>
                    </List>
                    <Divider />
                    <Grid item xs={12} style={{padding: '10px'}}>
                        <TextField id="outlined-basic-email" label="Search" variant="outlined" fullWidth />
                    </Grid>
                    <Divider />
                    <List>
                        <ListItem button key="all" onClick={() => setRoom("all")}>
                            <ListItemIcon>
                                <Avatar alt="all" />
                            </ListItemIcon>
                            <ListItemText primary="Tous">Tous</ListItemText>
                        </ListItem>
                        {
                            rooms.filter(el => el.name != "all").map((el) => {
                                return (
                                    <ListItem button key={el.name} onClick={() => changeRoom(el.name)}>
                                        <ListItemIcon>
                                            <Avatar alt={el.user2.username} />
                                        </ListItemIcon>
                                        <ListItemText primary={el.user2.username}>{el.user2.username}</ListItemText>
                                        <ListItemText secondary="online" align="right"></ListItemText>
                                    </ListItem>
                                )
                            })
                        }
                    </List>
                </Grid>
                <Grid item xs={9} maxHeight="100%">
                    <List className="messageArea" sx={{ height: "82%", overflowY: "auto" }}>
                        {
                            messages[room].map((mes) => {
                                let position;
                                if (  userConnected.username === mes.author.username ) {
                                    position = "right";
                                } else {
                                    position = "left";
                                }
                                return (
                                    <ListItem key={mes.author.username+"Msg"+mes.date}>
                                        <Grid container>
                                            <Grid item xs={12}>
                                                <ListItemText align={position} primary={mes.author.username + " - "+ mes.content}></ListItemText>
                                            </Grid>
                                            <Grid item xs={12}>
                                                <ListItemText align={position} secondary={moment(mes.date).format("DD/MM/yy HH:mm")}></ListItemText>
                                            </Grid>
                                        </Grid>
                                    </ListItem>
                                )
                            })
                        }
                    </List>
                    <Divider />
                    <Grid container style={{padding: '20px'}}>
                        <Grid item xs={11}>
                            <TextField id="outlined-basic-email" label="Ecrivez votre message..." fullWidth onChange={processInput} value={msgToSend.content} />
                        </Grid>
                        <Grid align="right">
                            <IconButton aria-label="add" className="button-padding" onClick={sendMessage}><SendIcon /></IconButton>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Box>
    );
}