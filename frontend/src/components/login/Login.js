import { useState } from "react";
import { useNavigate } from "react-router-dom"
import { useDispatch } from "react-redux";
import { initSocket, updateUser } from "../../store/actions";
// import socket from '../../utils/socket';

import TextField from '@mui/material/TextField';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import './Login.css';

import { login } from '../../service/login.service';

export function Login() {

    let navigate = useNavigate();
    const dispatch = useDispatch();

    // Mock data
    const [ userLogin, setUserLogin ] = useState({
        username: "",
        password: "",
    });

    const processInput = (event) => {
        const target = event.currentTarget;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        let currentVal = userLogin;
        setUserLogin({...userLogin, [name]: value});
        currentVal[name]= value;
    }

    const submit = async () => {

        const aferLogin = (user) => {
            if (user?.login) {
                const userFormat = {
                    id: user.id,
                    username: user.login,
                    account: user.account,
                }
                dispatch(updateUser(userFormat));
                dispatch(initSocket(userLogin));
                navigate("/");
            } else {
                console.log("NTM");
            }
        }

        const logUser = {
            login: userLogin.username,
            pwd: userLogin.password
        }

        login(logUser, aferLogin);
    }

    return (
        <Card sx={{ maxWidth: "50%", marginTop: 32 }} className="center">
            <CardContent>
                <Typography variant="h5" component="div">
                    Connexion
                </Typography>
                <Box sx={{ flexDirection: 'column' }}>
                    <div> 
                        <TextField 
                            sx={{ marginTop: 6, width: "50%"}}
                            variant="outlined"
                            label="Identifiant" 
                            name="username"
                            onChange={processInput}
                            value={userLogin.username}
                        />
                    </div>
                    <div>
                        <TextField sx={{ marginTop: 6, width: "50%"}} label="Mot de passe" variant="outlined" name="password" type="password" onChange={processInput} value={userLogin.password} />
                    </div>
                </Box>
            </CardContent>
            <CardActions sx={{ flexDirection: 'column' }}>
                <Button sx={{ marginTop: 6, width: "50%" }} className="center" size="medium" variant="contained" onClick={submit}>Se connecter</Button>
                <Typography component="div" sx={{ marginTop: 6 }}>
                    Vous n'avez pas encore de compte ? <Button size="medium" variant="text" onClick={() => navigate("/signup")}>Inscrivez vous !</Button>
                </Typography> 
            </CardActions>
        </Card>
    )
}
