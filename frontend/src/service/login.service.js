import { apiURLUser } from "../utils/config";

const header = { 
    "Content-Type": "application/json"
}

export const login = async (user, callback) => {

    console.log("user: ", user);
    fetch(`${apiURLUser}/auth`, {
        method: "POST",
        headers: header,
        body: JSON.stringify(user)
    }).then( response => {
        return response.json();
    }).then( data => {
        console.log(data);
        callback(data);
    });
}