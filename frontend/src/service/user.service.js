import { apiURLUser } from "../utils/config";

const header = { 
    "Content-Type": "application/json",
}

// export const getUsers = (callback) => {
//     fetch(`${apiURLUser}/users`, {
//         method: "GET",
//     }).then( response => {
//         return response.json();
//     }).then( data => {
//         console.log(data);
//         callback(data);
//     });
// }

export const createUser = async (user, callback) => {
    console.log("user: ", user);
    fetch(`${apiURLUser}/user`, {
        method: "POST",
        headers: header,
        body: JSON.stringify(user)
    }).then( response => {
        return response;
    }).then( data => {
        console.log(data);
        callback(data);
    });
}