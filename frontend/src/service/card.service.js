import { apiURLCard, apiURLStore } from "../utils/config";

const header = { 
    "Content-Type": "application/json",
}

export const getCards = async (type, userID, callback) => {
    fetch(`${apiURLCard}/cards`, {
        method: "GET",
        headers: header
    }).then( response => {
        return response.json();
    }).then( data => {
        let res = [];
        if (type === 'buy') {
            res = data.filter(el => el.user === null);
        } else if ( type === 'sell') {
            res = data.filter(el => el.user == userID);
        }
        callback(res);
    });
}

export const actionCard = async (userId, cardId, action) => {
    console.log("user: ",userId,"\n card:", cardId,"\n", action);
    fetch(`${apiURLStore}/${action}`, {
        method: "POST",
        headers: header,
        body: JSON.stringify({
            user_id: userId,
            card_id: cardId
        }),
    }).then( response => {
        return response.json();
    }).then( data => {
        console.log(data);
    });
}