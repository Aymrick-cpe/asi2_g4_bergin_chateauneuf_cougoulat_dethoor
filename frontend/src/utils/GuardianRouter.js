import { React } from "react";
import { useSelector } from "react-redux";
import { Navigate } from "react-router-dom"

export function GuardianRouter({ children }) {
    const userConnected = useSelector(state => state.userReducer.user);

    const isConnected = (user) => {
        // Check user not empty in store
        return !(Object.keys(user).length === 0 && user.constructor === Object)
    }

    const getElement = () => {
        if (isConnected(userConnected)) {
            return children;
        } else {
            return (<Navigate to="/login" />)
        }
    }

    return getElement();
}
