import { io } from "socket.io-client";

export const updateUser = (user) => {
    return { 
        type: 'UPDATE_USER_ACTION', 
        user: user 
    };
}

export const initSocket = (user) => {

    const URL = "http://localhost:8000";
    let socket = io(URL);

    // socket.onAny((event, ...args) => {
    //     console.log(event, args);
    // });

    // socket.auth = user.surname;
    // socket.connect();
    // console.log("CONNECTION");

    socket.emit('login', { username: user.username })

    return {
        type: 'INIT_SOCKET',
        io: socket,
    }
}