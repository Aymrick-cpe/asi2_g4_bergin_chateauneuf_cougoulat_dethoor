import { combineReducers } from 'redux';
import socketReducer from './socket';
import userReducer from './user';

const globalReducer = combineReducers({
    userReducer: userReducer,
    socketReducer: socketReducer,
});

export default globalReducer;
