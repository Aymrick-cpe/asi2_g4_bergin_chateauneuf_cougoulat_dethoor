const socketReducer = (state = {io:{}}, action) => {
    switch (action.type) {
        case 'INIT_SOCKET':
            return {io:action.io};
        default :
            return state;
    }
}

export default socketReducer;
