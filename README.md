# Membres du projet, groupe N°4

- Clément Dethoor
- Emile Bergin
- Aymrick Cougoulat
- Grégoire Chateauneuf

# Activités réalisées (par personne)

- Clément Dethoor: Backend archi micro services / Proxy
- Emile Bergin: Frontend React / Backend archi micro services
- Aymrick Cougoulat: Frontend React / Socket en NodeJS
- Grégoire Chateauneuf: Backend archi micro services / Gestion du rendu

# Activités réalisées

## Frontend
- Liaison avec le Backend
- Chat avec un systême de salon permettant aux utilisateurs de communiquer entre eux

## Backend
- Proxy
- Micro service Card ( testé et fonctionnel )
- Micro service Strore ( testé et fonctionnel )

## Socket NodeJS
- Création de différentes instances de socket
- Echanges entre une même personne avec une autre ou tous les autres

## Intégration continue
- Container Docker

# Activités non réalisées
- Création d'une session de jeu
- Jeu
- Historique du chat entre deux personnes

# lien Git

https://gitlab.com/Aymrick-cpe/asi2_g4_bergin_chateauneuf_cougoulat_dethoor

# Lien vidéo youtube

https://www.youtube.com/watch?v=jxArcblASRY